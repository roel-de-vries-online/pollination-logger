namespace PollinationLogger.Models
{
    public enum FlowerColor
    {
        LightPurple,
        PurpleAndWhite,
        Purple,
        White,
        DarkPurple
    }
}