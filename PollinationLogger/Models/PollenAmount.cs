namespace PollinationLogger.Models
{
    public enum PollenAmount
    {
        None,
        AlmostNothing,
        Little,
        Much,
        ExtremelyMuch,
        NoData
    }
}