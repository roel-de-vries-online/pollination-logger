# pollination-logger
An PWA to help logging data during plant pollination activites.

# pollination-logger
An application written for my girlfriend (Jessica Burger) to make the logging of data during her internship easier.

## License
Copyright Roel de Vries - All Rights Reserved

Unauthorized copying of this file, via any medium is strictly prohibited

Proprietary and confidential

Written by Roel de Vries - [email](https://mailhide.io/e/XLapyzGO), July 4 2021
